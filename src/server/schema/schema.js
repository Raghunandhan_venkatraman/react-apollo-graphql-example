//Please add all the schema at this place for now

const typeDefs = `
type todoType{
        _id:ID!,
        taskName:String        
    }
    
    input todoInputType{
        taskName:String    
    }
    
    type User {
        id: String!,
        email: String!,
        password: String!,
        token: String!
    }
    
  type Query {
    multipleTodos: [todoType]
    singleTodo(taskName:String!): [todoType]
  }
  
  type Mutation{
    addUser (
      newTodo: todoInputType!
    ): todoType
    updateUser(id:ID!,updatedTodo:todoInputType!):  todoType
    deleteUser(
         id: ID!
    ): todoType
    createUser(
        email: String!,
        password: String!,
        token: String!
    ): User
  }
  
  type Subscription{
    addedTask: todoType
  }
`;
export default typeDefs;