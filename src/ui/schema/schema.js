export const typeDefs = `
type Task {
   _id: ID!                # "!" denotes a required field
   taskName: String
}
# This type specifies the entry points into our API. In this case
# there is only one - "channels" - which returns a list of channels.
type Query {
   Tasks: [Task]    # "[]" means this is a list of channels
}
`;