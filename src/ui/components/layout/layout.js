import React,{Component} from 'react';
import ReactDOM from 'react-dom';
import logo from '../../../logo.svg';
import '../App/App.css';
import {Link} from 'react-router-dom'
class MainLayout extends Component{
    constructor(props){
        super(props);
    }


    render(){
        return (
            <div className="App">
                <nav>
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/addTask'>Add Task</Link></li>

                    </ul>
                </nav>
                {this.props.children}
            </div>

        );
    }
}

export default MainLayout;