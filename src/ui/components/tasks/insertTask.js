import React, {Component} from "react";
import axios from 'axios';
import { graphql,gql } from 'react-apollo'
import {Redirect} from "react-router";
class InsertTask extends Component{
    constructor(props){
        super(props);
        this.insertTaskData = this.insertTaskData.bind(this);
        this.state = {
            task: ''
        }
    }

    insertTaskData(e) {
        e.preventDefault();

        var taskName = this.refs.task.value;
        var that = this;
        this.props.addUser({taskName})
            .then(() => {
            console.log("done");

                that.props.history.push('/')


            })

    }

    render(){
        return (
            <form id="insertTaskData" ref="insertTaskData" onSubmit={this.insertTaskData}>
                <input type="text" ref="task" onChange={(e) => this.setState({"task":e.target.value})}/>
                <input type="submit" />
            </form>
        )
    }
}
const addMutation = gql`
  mutation addUser($taskName: String!) {
    addUser(newTodo:{taskName: $taskName}) {
      taskName
    }
  }
`;

//export default InsertTask;
export default graphql(addMutation, {
    props: ({ ownProps, mutate }) => ({
        addUser: ({ taskName }) =>
            mutate({
                variables: { taskName },
            })
    })
})(InsertTask)