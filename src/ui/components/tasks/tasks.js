import React,{ Component } from 'react';
import axios from 'axios';
import { graphql,gql } from 'react-apollo'
import VAL from './subscription.graphql';
class Tasks extends  Component{
    constructor(props){
        super(props);
        this.state = {
            "tasksData": []
        };
    }
    componentWillUpdate(){
        //this.loadTasks();
        this.props.data.refetch();


    }
    componentDidMount() {
        this.props.data.subscribeToMore({
            document: task_subscription,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) {
                    return prev;
                }
                const message = subscriptionData.data.addedTask;
                const msgs = prev.multipleTodos.concat(message);
                return {
                    multipleTodos: msgs
                }
            }
        });
    }

    getTasks(){
        var filteredTasks =  this.props.data.multipleTodos;
        if(filteredTasks){
            return filteredTasks.map((value)=>{
                return (
                    <li>{value.taskName}</li>
                )
            })
        }

    }
    render(){
        return (
            <ul>
                {this.getTasks()}

            </ul>
        )
    }
}

const task_subscription = gql`
    subscription onAddedTask{
      addedTask{
        _id,
        taskName
      }
    }
`;
const FeedQuery = gql`query multipleTodos {
  multipleTodos {
    _id
    taskName
  }
}`;


export default graphql(FeedQuery)(Tasks);
/*
export default Tasks;*/
