import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import Routes from './ui/routes/router.js';
import { BrowserRouter } from 'react-router-dom'
import { Router } from 'react-router'
import {createBrowserHistory} from 'history';
import {
    ApolloClient,
    ApolloProvider,
    createNetworkInterface
} from 'react-apollo';
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';

const networkInterface = createNetworkInterface({
    uri: 'http://localhost:4002/graphql'
});
const wsClient = new SubscriptionClient(`ws://localhost:5000/`, {
    reconnect: true
});


// Extend the network interface with the WebSocket
const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
    networkInterface,
    wsClient
);
// Finally, create your ApolloClient instance with the modified network interface
const client = new ApolloClient({
    networkInterface: networkInterfaceWithSubscriptions
});

networkInterface.use([{
    applyMiddleware(req, next) {
        if (!req.options.headers) {
            req.options.headers = {};  // Create the header object if needed.
        }
        req.options.headers['authorization'] = "hello";
        next();
    }
}]);

var history = createBrowserHistory()

ReactDOM.render(
    <ApolloProvider client={client}>
        <Router history={history}>
            <Routes history={history}/>
        </Router>
    </ApolloProvider>,
    document.getElementById('root')
);
registerServiceWorker();
